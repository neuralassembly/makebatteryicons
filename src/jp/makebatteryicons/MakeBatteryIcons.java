package jp.makebatteryicons;


import java.io.File;

import java.io.IOException;

import java.util.List;
import java.util.ArrayList;

import java.net.URI;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FlowLayout;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DnDConstants;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import java.awt.image.BufferedImage;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.imageio.ImageIO;


public class MakeBatteryIcons  extends JFrame  {
	
	private static final boolean DEBUG=false;
	
	private static final int THEME_AGE = 0x00;
	private static final int THEME_MAMI = 0x01;
	private static final int THEME_SAYAKA = 0x02;
	private static final int THEME_CUREMARCH = 0x03;

	private static final int THEME_AGE_NS = 0x04;
	private static final int THEME_MAMI_NS = 0x05;
	private static final int THEME_SAYAKA_NS = 0x06;
	private static final int THEME_CUREMARCH_NS = 0x07;
	
	private int theme = THEME_MAMI_NS;

	int w = 64;
	int h = 64;
	// Width of Rectangle border
	int bw = 6; 
	// Font size for battery != 100%
	int fs0 = 44; 
	// Font size for battery 100%
	int fs1 = 34;
	
	Color col0, col1, col2;
	Color bcol0, bcol1, bcol2;
	Color tcol0, tcol1, tcol2;
	Color col_charged, bcol_charged, tcol_charged;
	String fontname;
	

	MakeBatteryIcons() {
        getContentPane().setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("MakeBatteryIcons");
        
        JLabel label = new JLabel();
        label.setText("<html>保存先のフォルダ、あるいはその中の１ファイルを<br>ドラッグアンドドロップしてください。<br>バッテリーアイコンの生成が開始されます</html>");
        label.setHorizontalAlignment(JLabel.CENTER);
        BorderLayout l = new BorderLayout();
        setLayout(l);
        add(label);
        
        setSize(350, 200);
        setVisible(true);
        
    	switch(theme){
			case THEME_AGE:
				// 30-100%
				col0 = new Color(0,0,0,255); // black (Rectangle)
				bcol0 = new Color(0,0,0,255); // black (Border)
				tcol0 = new Color(56,254,238,255); // AGE's bright color (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(255,255,0,255); // Yellow (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fs0 = 54; 
				fs1 = 36;
				
				fontname = "Arial";
				break;
			case THEME_MAMI:
				// 30-100%
				col0 = new Color(213,191,119,255); // Mami's yellow (Rectangle)
				bcol0 = new Color(213,191,119,255); // Mami's yellow (Border)
				tcol0 = new Color(77,62,55,255); // Mami's brown (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(0,0,0,255); // Black (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fontname = "MadokaLetters";
				break;
			case THEME_SAYAKA:
				// 30-100%
				col0 = new Color(121,147,173,255); // Sayaka's dark blue (Rectangle)
				bcol0 = new Color(121,147,173,255); // Sayaka's dark blue (Border)
				tcol0 = new Color(7,17,82,255); // Sayaka's blue (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(255,255,0,255); // Yellow (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fontname = "MadokaLetters";
				break;
			case THEME_CUREMARCH:
				// 30-100%
				col0 = new Color(199,216,159,255); // pale green (Rectangle)
				bcol0 = new Color(199,216,159,255); // pale green(Border)
				tcol0 = new Color(34,88,65,255); // dark green (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(255,255,0,255); // Yellow (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fs0 = 54; 
				fs1 = 36;
				fontname = "Arial";
				break;
				/////
			case THEME_AGE_NS:
				// 30-100%
				col0 = new Color(0,0,0,255); // black (Rectangle)
				bcol0 = new Color(0,0,0,255); // black (Border)
				tcol0 = new Color(56,254,238,255); // AGE's bright color (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(255,255,0,255); // Yellow (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fs0 = 36; 
				fs1 = 24;
				
				fontname = "Arial";
				break;
			case THEME_MAMI_NS:
				// 30-100%
				col0 = new Color(213,191,119,255); // Mami's yellow (Rectangle)
				bcol0 = new Color(213,191,119,255); // Mami's yellow (Border)
				tcol0 = new Color(77,62,55,255); // Mami's brown (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(0,0,0,255); // Black (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fs0 = 35; 
				fs1 = 27;
				
				fontname = "MadokaLetters";
				break;
			case THEME_SAYAKA_NS:
				// 30-100%
				col0 = new Color(121,147,173,255); // Sayaka's dark blue (Rectangle)
				bcol0 = new Color(121,147,173,255); // Sayaka's dark blue (Border)
				tcol0 = new Color(7,17,82,255); // Sayaka's blue (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(255,255,0,255); // Yellow (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fs0 = 35; 
				fs1 = 27;
				
				fontname = "MadokaLetters";
				break;
			case THEME_CUREMARCH_NS:
				// 30-100%
				col0 = new Color(199,216,159,255); // pale green (Rectangle)
				bcol0 = new Color(199,216,159,255); // pale green(Border)
				tcol0 = new Color(34,88,65,255); // dark green (Text)
				
				// 10-29%
				col1 = new Color(255,255,0,255); // Yellow (Rectangle)
				bcol1 = new Color(255,255,0,255); // Yellow (Border)
				tcol1 = new Color(0,0,0,255); // Black (Text)
				
				// 1-9%
				col2 = new Color(255,0,0,255);   // Red (Rectangle)
				bcol2 = new Color(255,0,0,255); // Red (Border)
				tcol2 = new Color(255,255,255,255); // White (Text)
				
				// Color of rectangle when charged
				col_charged = new Color(0,0,0,255); // black(Rectangle)
				bcol_charged = new Color(0,0,0,255);  // black (Border)
				tcol_charged = new Color(255,255,255,255); // white (Text)
				
				fs0 = 36; 
				fs1 = 24;
				fontname = "Arial";
				break;
    	}
        
        
        DropTargetListener dtl = new DropTargetAdapter() {
            @Override public void dragOver(DropTargetDragEvent dtde) {
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)
                		|| dtde.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    dtde.acceptDrag(DnDConstants.ACTION_COPY);
                    return;
                }
                dtde.rejectDrag();
            }
            @Override
            public void drop(DropTargetDropEvent dtde) {
                try{
                    if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)
                    		|| dtde.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                        dtde.acceptDrop(DnDConstants.ACTION_COPY);
                        Transferable transferable = dtde.getTransferable();
                        List<File> list = null;
                        if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){ // Windows
                        	list = ((List<File>)transferable.getTransferData(DataFlavor.javaFileListFlavor));
                        }else if(dtde.isDataFlavorSupported(DataFlavor.stringFlavor)){ // Linux
                        	String str = (String) transferable.getTransferData(DataFlavor.stringFlavor);
                        	String lineSep = System.getProperty("line.separator");
                        	String[] fileList = str.split(lineSep);
                        	list = new ArrayList<File>();
                        	for(int i=0 ; i<fileList.length ; i++){
                        		try{
                        			File f = new File(new URI(fileList[i].replaceAll("[\r\n]", "")));
                        			list.add(f);
                        			if(DEBUG) System.err.println(f.getAbsolutePath());
                        		}catch(Exception e){}
                        	}
                        }
                        for(Object o: list) {
                            if(o instanceof File) {
                                File file = (File) o;     
                                if(file.isDirectory()){
                                	makeFiles(file);
                                }else{
                                	makeFiles(file.getParentFile());
                                }
                            }
                            break;
                        }
                        dtde.dropComplete(true);
                        return;
                    }
                }catch(UnsupportedFlavorException ufe) {
                    ufe.printStackTrace();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                dtde.rejectDrop();
            }
        };

        new DropTarget(this, DnDConstants.ACTION_COPY, dtl, true);
        
    }
	
	private void makeFiles(File dir){

		for(int i=1 ; i<=100 ; i++){
			File newfile = new File(dir.getAbsolutePath()
 				+ File.separator +"battery_"+i+".png");
			
			// System.err.println(newfile.getAbsolutePath());  

			Color col, bcol, tcol;
			int fs;
			
			if(i>=30){
				col = col0;
				bcol = bcol0;
				tcol = tcol0;
			}else if(i>=10){
				col = col1;
				bcol = bcol1;
				tcol = tcol1;
			}else{
				col = col2;
				bcol = bcol2;
				tcol = tcol2;
			}
			
			if(i!=100){
				fs = fs0;
			}else{
				fs = fs1;
			}

			BufferedImage writeImage = new BufferedImage(w, h, 
   				 BufferedImage.TYPE_4BYTE_ABGR);
			
			Graphics2D g = writeImage.createGraphics();
		    // Enable antialiasing for shapes
		    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		                         RenderingHints.VALUE_ANTIALIAS_ON);
		    // Enable antialiasing for text
		    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
		                         RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    		
		    // draw fill Rect
		    g.setColor(col);
		    g.fillRect(0,0, w,h);
		    
			BasicStroke wideStroke = new BasicStroke(bw);
			g.setStroke(wideStroke);
			g.setColor(bcol);
			g.drawRect(0,0, w-1,h-1);
			
			// Draw Digits
	        String str = ""+i;
	        g.setColor(tcol);
	        Font font = new Font(fontname, Font.BOLD, fs);
	        g.setFont(font);
	        FontMetrics fo = g.getFontMetrics();
            int strWidth = fo.stringWidth(str);
            int strHeight = (8*fo.getAscent()/10);
	        g.drawString(str, (w-strWidth)/2, (h+strHeight)/2);

			try{
				ImageIO.write(writeImage, "png", newfile);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		for(int i=1 ; i<=100 ; i++){
			File newfile = new File(dir.getAbsolutePath()
 				+ File.separator +"battery_plugged_"+i+".png");
			
			// System.err.println(newfile.getAbsolutePath());  

			Color col, bcol, tcol;
			int fs;
			
			col = col_charged;
			bcol = bcol_charged;
			tcol = tcol_charged;
			
			if(i!=100){
				fs = fs0;
			}else{
				fs = fs1;
			}

			BufferedImage writeImage = new BufferedImage(w, h, 
   				 BufferedImage.TYPE_4BYTE_ABGR);
			
			Graphics2D g = writeImage.createGraphics();
		    // Enable antialiasing for shapes
		    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		                         RenderingHints.VALUE_ANTIALIAS_ON);
		    // Enable antialiasing for text
		    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
		                         RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    		
		    // draw fill Rect
		    g.setColor(col);
		    g.fillRect(0,0, w,h);
		    
			BasicStroke wideStroke = new BasicStroke(bw);
			g.setStroke(wideStroke);
			g.setColor(bcol);
			g.drawRect(0,0, w-1,h-1);
			
			// Draw Digits
	        String str = ""+i;
	        g.setColor(tcol);
	        Font font = new Font(fontname, Font.BOLD, fs);
	        g.setFont(font);
	        FontMetrics fo = g.getFontMetrics();
            int strWidth = fo.stringWidth(str);
            int strHeight = (8*fo.getAscent()/10);
	        g.drawString(str, (w-strWidth)/2, (h+strHeight)/2);

			try{
				ImageIO.write(writeImage, "png", newfile);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	
	}

	
    public static void main(String [] args) {
        new MakeBatteryIcons();
    }


}
